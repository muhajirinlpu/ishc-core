#include <dht.h>

dht DHT;

#define DHT11_PIN 7

const int RainSensorMin = 0;
const int RainSensorMax = 1024;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int chk = DHT.read11(DHT11_PIN);

  int RainSensorReading = analogRead(A0);

  int LightSensorReading = analogRead(A1);

  int range = map(RainSensorReading, RainSensorMin, RainSensorMax, 0, 3);

  String RainStatus = "";

  switch (range) {
     case 0:
        RainStatus = "Flood";
        break;
     case 1:
        RainStatus = "Rain";
        break;
     case 2:
        RainStatus = "Dry";
        break;
  }
  
  Serial.println(
    "{\"humidity\" : \"" + String(DHT.humidity) + "\","
    + "\"temperature\" : \"" + String(DHT.temperature) + "\","
    + "\"light\" : \"" + String(LightSensorReading) + "\","
    + "\"rain\" : \"" + RainStatus
    + "\"}"
  );
  
  delay(2000);
}
