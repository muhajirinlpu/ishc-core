/**
 * APPLICATION BOOT
 * initializing global variable here .
 */
global['env'] = require('../env');
global['Serial'] = require('serialport');
global['GPIO'] = require('onoff').Gpio;
global['axios'] = require('axios');
global['Omx'] = require('node-omxplayer');

const ioredis = require('ioredis');

global['redis'] = new ioredis();
global['support'] = require('./support');

const event = require('./event-handler');

event.start();
