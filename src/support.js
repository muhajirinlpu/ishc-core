module.exports = {
    report: function (message) {
        axios.post(env.server_host+':'+env.http_port+'/report/'+ env.uuid +'/store', {
        	'report' : message
        }).then(function(message) {
        	//
        }).catch(function(err) {
        	//	
        });
    },
    inArray: function (needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}
};