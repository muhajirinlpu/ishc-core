global.io = require('socket.io-client');

const socket = io(env.server_host + ":" + env.socket_port);
const arduino = require('./modules/arduino');
const magic_door = require('./modules/magic-door');
const magic_lamp = require('./modules/magic-lamp');
const laundry = require('./modules/laundry');
const guest = require('./modules/guest');

console.log(env.server_host + ":" + env.socket_port);

module.exports = {
    start: function () {
        // Boot runtime who need run at first
        this.boot();
        // Registering service listening from server
        socket.on('connect', function() {
            console.log('Device connected with ID : ' + socket.id)
        });

        socket.on('private-raspberry-pi-' + env.uuid , function(data) {
            redis.set(data.key, JSON.stringify(data.value));
            // run needed check module
            console.log(data);
            magic_lamp.check();
        });
    },

    boot : function() {
        arduino.run();
        magic_door.run();
        magic_lamp.run();
        laundry.run();
        guest.run();
    }
};
