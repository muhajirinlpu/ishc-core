const solenoid = new GPIO(env.relay.solenoid, 'out');
const mfrc522 = require("mfrc522-rpi");
// 

var Door = {
    run: function () {
        setInterval(function(){
            mfrc522.init();

            let response = mfrc522.findCard();
            if (!response.status) {
                // No card
                return;
            }

            response = mfrc522.getUid();
            if (!response.status) {
                // UID scan error
                return;
            }

            let uid = response.data;
            uid = uid[0].toString(16) + uid[1].toString(16) + uid[2].toString(16) + uid[3].toString(16);

            Door.auth(uid);

            mfrc522.stopCrypto();

        }, 500);
    },
    auth: function (serial) {
        redis.get('door', function (err, result) {
            if (!err) {
                console.log(serial);
                if (support.inArray(serial, JSON.parse(result).key)) {
                    Door.unlock();
                    support.report("Unlocked key using RFID with id " + serial);
                } else {
                    support.report("Someone was try to unlock using wrong tag id " + serial);
                    axios.post(env.server_host+':'+env.http_port+'/door/'+ env.uuid +'/store', {
                        'identity' : serial
                    }).then(function(message) {
                        //
                    }).catch(function(err) {
                        //  
                    });
                }
            }
        });
    },
    unlock: function () {
        var me = this;
        var locked = setTimeout(function() {
            me.lock();
            clearTimeout(locked);
        }, 5000);

        return solenoid.writeSync(0);
    },
    lock: function () {
        return solenoid.writeSync(1);
    }
};

module.exports = Door;