const Laundry = {
    stepper: [],
    statement: false,
    commander: false,
    pattern: {
        'back' : [
            [ 0, 0, 0, 1 ],
            [ 0, 0, 1, 1 ],
            [ 0, 0, 1, 0 ],
            [ 0, 1, 1, 0 ],
            [ 0, 1, 0, 0 ],
            [ 1, 1, 0, 0 ],
            [ 1, 0, 0, 0 ],
            [ 1, 0, 0, 1 ],
            [ 0, 0, 0, 0 ]
        ],
        'forward' : [
            [ 0, 0, 0, 0 ],
            [ 1, 0, 0, 1 ],
            [ 1, 0, 0, 0 ],
            [ 1, 1, 0, 0 ],
            [ 0, 1, 0, 0 ],
            [ 0, 1, 1, 0 ],
            [ 0, 0, 1, 0 ],
            [ 0, 0, 1, 1 ],
            [ 0, 0, 0, 1 ]
        ]
    },
    prepare : function () {
        for (let pin of env.stepper) {
            this.stepper.push(new GPIO(pin, 'out'));
        }
    },
    run: function () {
        this.prepare();
        redis.get('laundry', function (err, result) {
            Laundry.statement = JSON.parse(result);
        });

        global['laundry'] = setInterval(function () {
            redis.get('laundry', function (err, result) {
                let laundry_data = JSON.parse(result);
                const schedule = JSON.parse(result).schedule.split(":");

                let now = new Date();
                let off_schedule = new Date(now.getFullYear(), now.getMonth(), now.getDate(),
                    schedule[0], schedule[1]);

                // Turn off raspi while clock is set
                if (now > off_schedule) {
                    axios.post(env.server_host+':'+env.http_port+'/laundry/'+ env.uuid +'/turn_off')
                        .then(function(message) {}).catch(function(err) {});

                    redis.set('laundry', JSON.stringify({
                        status: 0,
                        schedule: JSON.parse(result).schedule
                    }));

                    laundry_data.status = 0;
                }

                // checking for rain sensor
                redis.get('rain', function(err, result) {
                    if (result == 'Rain') {
                        laundry_data.status = 0;
                    }
                    /*console.log(laundry_data);
                    console.log(Laundry.statement);
                    console.log(Laundry.commander);*/
                    Laundry.commander = laundry_data;
                    Laundry.check();
                });

            });
        }, 1000);
    },
    check: function () {
        if (Laundry.statement.status !== Laundry.commander.status) {
            console.log(Laundry.commander);
            // prevent turn on while rain
            redis.get('rain', function(err, result) {
                if (result == 'Rain' && Laundry.commander.status == 1) {
                    Laundry.commander.status = 0;
                }
                
                Laundry.command(Laundry.commander);
                Laundry.statement = Laundry.commander;
            });
        }
    },
    command: function (data) {
        if (data.status == 1) {
            this.forward();
        } else {
            this.back();
        }
    },
    back: function () {
        this.spin(this.pattern['back'], 3000);
    },
    forward: function () {
        this.spin(this.pattern['forward'], 3000);
    },
    spin: function (pattern, period) {
        let circling = 0;
        let timer = 0;
        const loop = setInterval(function () {
            let step = pattern[timer];
            for (let i = 0; i < step.length; i++) {
                Laundry.stepper[i].writeSync(step[i]);
            }
            circling++;
            timer++;

            if (timer === 8) { timer = 0 }
            if (circling > period) { 
                clearInterval(loop);
                Laundry.stop();
            }
        }, 1);
    },
    stop: function() {
        for (let i = 0; i < Laundry.stepper.length; i++) {
            Laundry.stepper[i].writeSync(0);
        }
    }
};

module.exports = Laundry;