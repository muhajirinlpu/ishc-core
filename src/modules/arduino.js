
const Sensor = {
    run: function () {
        const port = new Serial(env.arduino_serial);
        const Readline = Serial.parsers.Readline;

        const parser = port.pipe(new Readline());

        let data_count = 0;

        parser.on('data', function (data){
            if (data_count >= 10) {
                var data = JSON.parse(data);
                Sensor.check(data);
            }
            
            data_count++
        });
    },
    check: function (object) {
        for (let key in object) {
            if (object.hasOwnProperty(key))
                redis.get(key, function (err, result) {
                    if (result !== object[key]) {
                        redis.set(key, object[key]);
                        axios.post(env.server_host+':'+env.http_port+'/monitor/'+ env.uuid +'/store', {
                            'name' : key,
                            'value' : object[key]
                        }).then(function(message) {
                            //
                        }).catch(function(err) { });
                    }
                })
        }
    }
};

module.exports = Sensor;