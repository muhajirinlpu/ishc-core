const on = 0;
const off = 1;

module.exports = {
    pin: {},
    run: function () {
        for (let key in env.relay.lamp) {
            this.pin[key] = new GPIO(env.relay.lamp[key], 'out');
        }
        global.lamp_obj = this;
        setInterval(this.check, 5000)
    },
    check: function () {
        for (let key in env.relay.lamp) {
            let command = off;
            if (env.relay.lamp.hasOwnProperty(key)) {
                redis.get(key, function (err, result) {
                    let data = JSON.parse(result);
                    let pin = lamp_obj.pin[key];
                    // its not a bathroom
                    if (data['on_schedule'] !== undefined && data['off_schedule'] !== undefined) {
                        let now = new Date();
                        let on_schedule = new Date(now.getFullYear(), now.getMonth(), now.getDate(),
                            data['on_schedule'].split(":")[0], data['on_schedule'].split(":")[1]);
                        let off_schedule = new Date(now.getFullYear(), now.getMonth(), now.getDate(),
                            data['off_schedule'].split(":")[0], data['off_schedule'].split(":")[1]);

                        if (on_schedule.getHours() < off_schedule.getHours())
                            on_schedule = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1,
                                data['off_schedule'].split(":")[0], data['off_schedule'].split(":")[1]);

                        if (now > off_schedule && now < on_schedule)
                            command = off;
                        else if (now > on_schedule)
                            command = on;

                    } else {
                        // its a bathroom
                        data ? command = on : command = off;
                    }

                    pin.writeSync(command)
                });
            }
        }
    },
};
