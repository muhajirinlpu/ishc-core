module.exports = {
    server_host : 'http://144.217.104.56',
    socket_port : 6001,
    http_port : 80,
    arduino_serial : '/dev/ttyACM0',
    uuid : 'a28b92d0-a723-11e7-862a-41113311deea',
    relay : {
        lamp : {
            terrace : 17,
            room_1 : 27,
            room_2 : 22,
            living_room : 18,
            bathroom : 23
        },
        solenoid : 24
    },
    stepper : [26, 16, 20, 21],
    bel: 12
};
